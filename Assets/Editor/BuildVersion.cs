﻿using UnityEngine;
using System.IO;

namespace Wild {

public class BuildSupport : MonoBehaviour {
    // Location of the app that runs the web request.
//    private static string GurlPath = @"tooling\gurl.exe";
    private static string GurlPath = @"curl";

    public static void PreExport() {
		GenerateVersionFile();
	}

	/// <summary>
	/// Create a file that contains my version number, for the build system.
	/// </summary>
    private static void GenerateVersionFile() {
		string version = string.Empty;
		version = GetAppVersion();
		if (!string.IsNullOrEmpty(version)) {
			try {
				string dst_path = Path.Combine("Assets", "StreamingAssets");
				dst_path = Path.Combine(dst_path, "version.txt");
				string[] lines = { "{\"version\": \"" + version + "\"}" };
				File.WriteAllLines(dst_path, lines);
			} catch (System.Exception ex) {
				Debug.Log("CloudBuid.PreExport() error writing version file: " + ex.Message);
			}
		}
	}
    

	private static string GetAppVersion() {
		return Application.version;
	}
}
}